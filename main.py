from os.path import dirname

from six import iteritems, print_

from placeandroute.tilebased.parallel import ParallelPlacementHeuristic
from placeandroute.tilebased.chimera_tiles import chimera_tiles, expand_solution
import logging
from placeandroute.problemgraph import parse_cnf
from multiprocessing import Pool
import dwave_networkx as dwnx

from placeandroute.tilebased.utils import cnf_to_constraints, show_result

import argparse

parser = argparse.ArgumentParser(description='Execute the fracos2017 examples on a DWave quantum annealer')
parser.add_argument('input', help='Name of the problem you want to test (eg. .cnf)')
parser.add_argument('--outputimage', help='Name of the output file where you store a snapshot of the encoding (eg. .svg or .png)')


if __name__ == '__main__':
    args = parser.parse_args()
    if args.outputimage is not None and (args.outputimage[-3:] != "svg" and args.outputimage[-3:] != "png"):
        print("Output should have .svg or .png format")
        exit()
    logging.basicConfig(level=logging.INFO, format='%(levelname)s %(asctime)s %(processName)s %(message)s')

    #open 3-sat problem file
    with open(args.input, "r") as f:
        cnf = (parse_cnf(f))

    #prepare constraints
    constraints = list(cnf_to_constraints(cnf, max(max(x) for x in cnf)))

    #prepare tile graph
    chimera_size = 16
    tile_graph, choices = chimera_tiles(chimera_size, chimera_size)

    #initialize and run heuristic
    heuristic = ParallelPlacementHeuristic(constraints, tile_graph, choices)
    pool = Pool()
    success = heuristic.par_run(pool, stop_first=True)
    #success = heuristic.run(stop_first=False)


    #print results
    if success:
        print_("Success")
        # constraint_placement is a map from constraint to tile
        for c, t in iteritems(heuristic.constraint_placement):
            print_(c, t)

        print_("Expanding chains")
        # heuristic.chains maps from variable to tile, expand to a map variable->qubit
        chains = expand_solution(tile_graph, heuristic.chains, dwnx.chimera_graph(chimera_size))

        print_(repr(chains))
        if args.outputimage is not None:
            show_result(chimera_size, chains, args.outputimage)
    else:
        print_("Failure")